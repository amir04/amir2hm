#include "Ribosome.h"

Protein* Ribosome::create_protein(string &RNA_transcript)const
{
	string check = "";
	Protein* tmp = new Protein;
	tmp->init();
	if (RNA_transcript.length() < 3)
	{
		return nullptr;
	}
		
	while (RNA_transcript.length() >= 3)
	{
		check = RNA_transcript;
		check.erase(3);
		RNA_transcript.erase(0, 3);
		if (get_amino_acid(check) == UNKNOWN)
		{
			tmp->clear();
			return nullptr;
		}
		else
		{
			tmp->add(get_amino_acid(check));
		} 
	}
	return tmp;
}