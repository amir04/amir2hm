#include "Cell.h"

void Cell::init(const std::string dna_sequence, const Gene glucose_receptor_gene)
{
	this->_glocus_receptor_gene = glucose_receptor_gene;
	this->_nucleus.init(dna_sequence);
	this->_mitochondrion.init();
}
bool Cell::get_ATP()
{
	string str = _nucleus.get_RNA_transcript(_glocus_receptor_gene);
	Protein* protein = new Protein;
	protein = _ribosome.create_protein(str);
	if (!protein)
	{
		cerr << "couldn't create protein" << endl;
		_exit(1);
	}
	_mitochondrion.insert_glucose_receptor(*protein);
	_mitochondrion.set_glucose(50);
	if (_mitochondrion.produceATP())
	{
		_atp_units = 100;
		return true;
	}
	return false;
}