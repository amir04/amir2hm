#include "AminoAcid.h"
#include "Mitochondrion.h"
#include "Nucleus.h"
#include "Protein.h"
#include "Ribosome.h"
using namespace std;

class Cell
{
public:
	/*the fuction get dna - string, gene and put this values in the private data of the class
	return- none
	*/
	void init(const std::string dna_sequence, const Gene glucose_receptor_gene);
	//the function return true if it can create atp, else false
	bool get_ATP();

private:
	Nucleus _nucleus;
	Ribosome _ribosome;
	Mitochondrion _mitochondrion;
	Gene _glocus_receptor_gene;
	unsigned int _atp_units;
};