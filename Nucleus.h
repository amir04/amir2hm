#pragma once
#include <iostream>
#include <string>
class Gene
{
public:
	void init(const unsigned int start, const unsigned int end, const bool on_complementary_dna_strand);

	//GETTERS:
	unsigned int get_start() const;
	unsigned int get_end() const;
	bool is_on_complementary_dna_strand() const;

	//SETTERS
	void  setstart(const unsigned int newstart);
	void  setend(const unsigned int newend);
	void  set_on_complementary_dna_strand(const bool new_on_complementary_dna_strand);

private:
	unsigned int _start;
	unsigned int _end;
	bool _on_complementary_dna_strand;

};

class Nucleus
{
public:
	/*i put value in the DNA, descript him 
	and also put value in his complementry
	*/
	void init(const std::string dna_sequence);

	//return string, i return the part of the dna by the data from the class gene
	std::string get_RNA_transcript(const Gene& gene) const;

	//return the reverese dna by using swap - type: string
	std::string get_reversed_DNA_strand() const;

	unsigned int get_num_of_codon_appearances(const std::string& codon) const;
private:
	std::string _DNA_strand;
	std::string _complementary_DNA_strand;
};
