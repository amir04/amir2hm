#pragma once
#include <string>
#include <iostream>
#include "Protein.h"


class Mitochondrion
{
public:
	//put 0 and false in the data
	void init();

	/*the function get protein, and if he like the reqirements
	return none
	*/
	void insert_glucose_receptor(const Protein & protein);

	void set_glucose(const unsigned int glocuse_units);

	//return true if it had glocuse recepter and have more than 50
	bool produceATP() const;

private:
	unsigned int _glocuse_level;
	bool _has_glocuse_receptor;
};
