#include "Nucleus.h"
#include <iostream>
#include <string>

void Gene::init(const unsigned int start, const unsigned int end, const bool on_complementary_dna_strand)
{
	this->_start = start;
	this->_end = end;
	this->_on_complementary_dna_strand = on_complementary_dna_strand;
}

unsigned int Gene::get_start() const
{
	return this->_start;
}

unsigned int Gene::get_end() const
{
	return this->_end;
}

bool Gene::is_on_complementary_dna_strand() const
{
	return this->_on_complementary_dna_strand;
}

void Gene::setstart(const unsigned int newstart)
{
	this->_start = newstart;
}

void Gene::setend(const unsigned int newend)
{
	this->_end = newend;
}

void Gene::set_on_complementary_dna_strand(const bool new_on_complementary_dna_strand)
{
	this->_on_complementary_dna_strand = new_on_complementary_dna_strand;
}

void Nucleus::init(const std::string dna_sequence)
{
	int i = 0;
	for (i = 0; i < dna_sequence.length(); i++)
	{
		if (dna_sequence[i] != 'A' && dna_sequence[i] != 'C' && dna_sequence[i] != 'G' && dna_sequence[i] != 'T')
		{
			std::cerr << "DNA needs to contain A, C, G, T only!" << std::endl;
			_exit(1);
		}
	}
	this->_DNA_strand = dna_sequence;
	this->_complementary_DNA_strand = "";
	for (i = 0; i < dna_sequence.length(); i++)
	{
		if (dna_sequence[i] == 'A')
		{
			this->_complementary_DNA_strand += 'T';
		}
		else if (dna_sequence[i] == 'C')
		{
			this->_complementary_DNA_strand += 'G';
		}
		else if (dna_sequence[i] == 'G')
		{
			this->_complementary_DNA_strand += 'C';
		}
		else if (dna_sequence[i] == 'T')
		{
			this->_complementary_DNA_strand += 'A';
		}
	}
}

std::string Nucleus::get_RNA_transcript(const Gene& gene) const
{
	int i = gene.get_start(), end = gene.get_end();
	std::string choosen_dna;
	std::string rna = "";

	//i check which dna i need to use and keep him in 
	if (gene.is_on_complementary_dna_strand())
	{
		choosen_dna = _complementary_DNA_strand;
	}
	else
	{
		choosen_dna = _DNA_strand;
	}
	//i run on this dna and take the rna(move T to U if there is  a needed
	for (i; i <= end; i++)
	{
		if (choosen_dna[i] == 'T')
		{
			rna += 'U';
		}
		else
		{
			rna += choosen_dna[i];
		}
	}
	return rna;
}

std::string Nucleus::get_reversed_DNA_strand() const
{
	std::string dna = this->_DNA_strand;
	int dna_length = this->_DNA_strand.length();
	//i run O(1/2n) and any time i swap the first with the last
	//if the length is 4 i will swap 0 && 3 and 1 %% 2 - indexes
	for (int i = 0; i < dna_length / 2; i++)
	{
		std::swap(dna[i], dna[dna_length - i - 1]);
	}

	return dna;
}

unsigned int Nucleus::get_num_of_codon_appearances(const std::string& codon) const
{
	int occurrences = 0;
	std::string::size_type start = 0;
	std::string dna = this->_DNA_strand;

	while ((start = dna.find(codon, start)) != std::string::npos) {
		++occurrences;
		start += codon.length();
	}
	return occurrences;
}
