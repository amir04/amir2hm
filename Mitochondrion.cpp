#include "Mitochondrion.h"
#include "AminoAcid.h"

void Mitochondrion::init()
{
	this->_glocuse_level = 0;
	this->_has_glocuse_receptor = false;
}

void Mitochondrion::insert_glucose_receptor(const Protein & protein)
{

	AminoAcidNode* first = new AminoAcidNode();
	first = protein.get_first();
	if (first->get_data() == ALANINE)
	{
		first = first->get_next();
		if (first->get_data() == LEUCINE)
		{
			first = first->get_next();
			if (first->get_data() == GLYCINE)
			{
				first = first->get_next();
				if (first->get_data() == HISTIDINE)
				{
					first = first->get_next();
					if (first->get_data() == LEUCINE)
					{
						first = first->get_next();
						if (first->get_data() == PHENYLALANINE)
						{
							first = first->get_next();
							if (first->get_data() == AMINO_CHAIN_END)
							{
								this->_has_glocuse_receptor = true;
								
							}
						}
					}
				}
			}
		}
	}
}

void Mitochondrion::set_glucose(const unsigned int glocuse_units)
{
	this->_glocuse_level = glocuse_units;
}

bool Mitochondrion::produceATP() const
{
	return ((this->_has_glocuse_receptor) && (this->_glocuse_level >= 50));
}